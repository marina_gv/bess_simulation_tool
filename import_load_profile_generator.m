clear
clc
project_name='PROJECT_NAME';

%%


T=readtable([project_name '_export_detail.csv']);

timestamp=datetime(T.Zeitstempel_in_Normalzeit,'Format','dd.MM.yyyy HH:mm');
Bezug=T.NettoBezugSolaroptimiert_in_kWh*4;
Ruckspeisung=-T.NettoRuckspeisungSolaroptimiert_in_kWh*4;
Erzeugung=T.PVErzeugung_in_kWh*4;

writetable(table(timestamp,Bezug,Ruckspeisung),'input data file.xlsx','Sheet','Bezug_Ruckspeisung',...
    'Range',['A2:C' num2str(height(T)+1)],'WriteVariableNames',false)

writetable(table(timestamp,Erzeugung),'input data file.xlsx','Sheet','PV profile',...
    'Range',['A2:B' num2str(height(T)+1)],'WriteVariableNames',false)



T=readtable([project_name '_eingabewerte.csv']);

HT_zeiten=nan(7,2);

aux=T{strcmp(T{:,1},'Startzeit Hochtarif Werktag:'),2};
HT_zeiten(1:5,1)=str2double(aux{1}(1:2));
aux=T{strcmp(T{:,1},'Endzeit Hochtarif Werktag:'),2};
HT_zeiten(1:5,2)=str2double(aux{1}(1:2));

aux=T{strcmp(T{:,1},'Startzeit Hochtarif Samstag:'),2};
HT_zeiten(6,1)=str2double(aux{1}(1:2));
aux=T{strcmp(T{:,1},'Endzeit Hochtarif Samstag:'),2};
HT_zeiten(6,2)=str2double(aux{1}(1:2));

aux=T{strcmp(T{:,1},'Startzeit Hochtarif Sonntag:'),2};
HT_zeiten(7,1)=str2double(aux{1}(1:2));
aux=T{strcmp(T{:,1},'Endzeit Hochtarif Sonntag:'),2};
HT_zeiten(7,2)=str2double(aux{1}(1:2));

writetable(table(repmat(HT_zeiten,1,3)),'input data file.xlsx','Sheet','HT_hours',...
    'Range','B2:G8','WriteVariableNames',false)

aux=T{strcmp(T{:,1},'Peakleistung PV [kWp]:'),2};


writetable(table(str2double(aux{1,1})),'input data file.xlsx','Sheet','PV profile',...
    'Range','C2','WriteVariableNames',false)