clear
clc
close all

%% all input data loading

[sc1,~,sc]=xlsread('input data file.xlsx','scenario');
ho=xlsread('input data file.xlsx','horizon');
C_P_b=xlsread('input data file.xlsx','battery parameter');

horizon_years=ho(1,ho(end,:)==1);
P_b_all = C_P_b(C_P_b(:,end)==1,1);
C_b_all = C_P_b(C_P_b(:,end)==1,2);

sc_name=sc(1,find(sc1(end,:)==1)+1);

horizon_years_all=ho(1,:);
C_b_gen=C_P_b(:,2);
P_b_gen=C_P_b(:,1);

ind_save=find(ismember(C_b_gen,C_b_all)==1);
for jj=1:length(sc_name)
    filepath=sc_name{jj}
    for k=1:length(horizon_years)
        horizon_year=horizon_years(k)
        for ii=1:length(P_b_all)
            
            P_b_max = P_b_all(ii);
            C_b = C_b_all(ii);
            
            scenario=['C_BESS_' num2str(C_b) '_P_BESS_' num2str(P_b_max) '_year_' num2str(horizon_year)]
            
            filename=scenario;
            load([filepath '/' filename])
            month_change=find(not(diff(months(1:T))==0));
            month_change=unique([month_change; 0; T]);
            
            cost_energy.(filename)=     (365*24*4)/T*(+ sum(P_gr_p_all(1,ht_ep(1:T)))*ts*c_ep_ht ...
                + sum(P_gr_p_all(1,nt_ep(1:T)))*ts*c_ep_nt ...
                + sum(P_gr_p_all(1,ht_gr(1:T)))*ts*c_gr_ht ...
                + sum(P_gr_p_all(1,nt_gr(1:T)))*ts*c_gr_nt ...
                - sum(P_gr_n_all(1,ht_in(1:T)))*ts*r_in_ht ...
                - sum(P_gr_n_all(1,nt_in(1:T)))*ts*r_in_nt);
            
            
            monthly_peaks=[];
            P_gr_peak_all(isnan(P_gr_peak_all))=0;
            for l=1:length(month_change)-1
                monthly_peaks=[monthly_peaks max(P_gr_p_all(1,month_change(l)+1:month_change(l+1))+B_prl_all(1,month_change(l)+1:month_change(l+1))*PRL_ppeak)];
            end
            avg_peak.(filename)=mean(monthly_peaks);
            cost_peak.(filename)=sum(monthly_peaks*c_peak)/length(monthly_peaks)*12;
            revenues_prl.(filename)=-sum(B_prl_all(1,1:T))*r_prl*ts;
            C_battery.(filename)=C_b;
            P_battery.(filename)=P_b_max;
            year_sim.(filename)=horizon_year;
            

            
            fig=figure;
            set(gcf,'Position',[40 200 2714*0.4 1182*0.4])
            plot(timestamp(1:T),P_load_f_all(1,1:T)/1000)
            hold on
            plot(timestamp(1:T),P_gr_p_all(1,1:T)/1000)
            ylabel('Leistung [MW]')
            legend('Profil ohne Batterie','Profil mit Batterie','Location','NorthOutside','Orientation','Horizontal')
            legend('boxoff')
            set(gca,'FontSize', 16);
            saveas(fig,[filepath '/' filename])
            saveas(fig,[filepath '/' filename],'tiffn')
            
        end
        
        %%
        scenario=['base_' num2str(horizon_year)]
        cost_energy.(scenario)=   (365*24*4)/T*(+ sum(P_load_all(ht_ep(1:T)))*ts*c_ep_ht ...
            + sum(P_load_all(nt_ep(1:T)))*ts*c_ep_nt ...
            + sum(P_load_all(ht_gr(1:T)))*ts*c_gr_ht ...
            + sum(P_load_all(nt_gr(1:T)))*ts*c_gr_nt ...
            - sum(P_gen_all(ht_in(1:T)))*ts*r_in_ht ...
            - sum(P_gen_all(nt_in(1:T)))*ts*r_in_nt);
        
        monthly_peaks=[];
        for l=1:length(month_change)-1
            monthly_peaks=[monthly_peaks max(P_load_all(month_change(l)+1:month_change(l+1)))];
            
        end
        cost_energy_grid.(scenario)=  (365*24*4)/T*(+ sum(P_load_all(ht_gr(1:T)))*ts*c_gr_ht ...
            + sum(P_load_all(nt_gr(1:T)))*ts*c_gr_nt);
        avg_peak.(scenario)=mean(monthly_peaks);
        cost_peak.(scenario)=sum(monthly_peaks*c_peak)/length(monthly_peaks)*12;
        revenues_prl.(scenario)=0;
        year_sim.(scenario)=horizon_year;
        
        fig=figure;
        set(gcf,'Position',[40 200 2714*0.4 1182*0.4])
        plot(timestamp(1:T),P_load_f_all(1,1:T)/1000,'k')
        ylabel('Leistung [MW]')
        legend('Profil ohne Batterie','Location','NorthOutside','Orientation','Horizontal')
        legend('boxoff')
        set(gca,'FontSize', 16);
        saveas(fig,[filepath '/' scenario])
        saveas(fig,[filepath '/' scenario],'tiffn')
               
    end
    
    %%
    xlswrite('results.xlsx',{'scenario'},filepath,['A' num2str(1)])
    xlswrite('results.xlsx',{'cost energy'},filepath,['B' num2str(1)])
    xlswrite('results.xlsx',{'cost peak'},filepath,['C' num2str(1)])
    xlswrite('results.xlsx',{'revenues PRL'},filepath,['D' num2str(1)])
    xlswrite('results.xlsx',{'avg peak'},filepath,['E' num2str(1)])
    xlswrite('results.xlsx',{'net rev energy'},filepath,['F' num2str(1)])
    xlswrite('results.xlsx',{'net rev peak'},filepath,['G' num2str(1)])
    xlswrite('results.xlsx',{'net rev PRL'},filepath,['H' num2str(1)])
    xlswrite('results.xlsx',{'avg peak reduction'},filepath,['I' num2str(1)])
    xlswrite('results.xlsx',{'C_b'},filepath,['J' num2str(1)])
    xlswrite('results.xlsx',{'P_b'},filepath,['K' num2str(1)])
    xlswrite('results.xlsx',{'year'},filepath,['L' num2str(1)])
    scen=fieldnames(C_battery);
    for ii=1:length(scen)
      if contains(scen{ii},num2str(horizon_years_all(1)))
          cost=0;
          j=ind_save(ii);
      elseif contains(scen{ii},num2str(horizon_years_all(2)))
          cost=6;
          j=ind_save(ii-length(ind_save));
      else
          cost=12;
          j=ind_save(ii-2*length(ind_save));
      end
        xlswrite('results.xlsx',scen(ii),filepath,['A' num2str(j+1+cost)])
        xlswrite('results.xlsx',cost_energy.(scen{ii}),filepath,['B' num2str(j+1+cost)])
        xlswrite('results.xlsx',cost_peak.(scen{ii}),filepath,['C' num2str(j+1+cost)])
        xlswrite('results.xlsx',revenues_prl.(scen{ii}),filepath,['D' num2str(j+1+cost)])
        xlswrite('results.xlsx',avg_peak.(scen{ii}),filepath,['E' num2str(j+1+cost)])
        xlswrite('results.xlsx',cost_energy.(['base_' num2str(year_sim.(scen{ii}))])-cost_energy.(scen{ii}),filepath,['F' num2str(j+1+cost)])
        xlswrite('results.xlsx',cost_peak.(['base_' num2str(year_sim.(scen{ii}))])-cost_peak.(scen{ii}),filepath,['G' num2str(j+1+cost)])
        xlswrite('results.xlsx',revenues_prl.(['base_' num2str(year_sim.(scen{ii}))])-revenues_prl.(scen{ii}),filepath,['H' num2str(j+1+cost)])
        xlswrite('results.xlsx',avg_peak.(['base_' num2str(year_sim.(scen{ii}))])-avg_peak.(scen{ii}),filepath,['I' num2str(j+1+cost)])
        xlswrite('results.xlsx',C_battery.(scen{ii}),filepath,['J' num2str(j+1+cost)])
        xlswrite('results.xlsx',P_battery.(scen{ii}),filepath,['K' num2str(j+1+cost)])
        xlswrite('results.xlsx',year_sim.(scen{ii}),filepath,['L' num2str(j+1+cost)])
    end
end