1) Optional: Importing profiles from load profile genererator
If you are inporting profiles from the load profile generator
- copy the "export_detail" and "eingabewerte" csv files from the load profile generator to the worksspace
- open import_load_profile_generator.m and write the project_name=...
- run import_load_profile_generator.m, this will automatically generate the inputs for the "input data file.xlsx" -> Bezug_Ruckspeisung and PV profile tabs.

2) Before you trigger simulation, fill in the excel file "input data file"

3) Run the simulation by running MAIN_BESS_template.m

4) To export the results to xlsx file, run analysis.m

5) Analyse the financial result in results.xlsx file
- Input Capex and Opex in the Tab Inputs 

