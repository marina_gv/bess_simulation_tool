cvx_begin quiet

if no_b_losses==1
    variable P_b(N,1)
else
    variable P_b_p(N,1) nonnegative
    variable P_b_n(N,1) nonnegative
end
if SOC_init_free==1
    variable SOC_init(1,1) nonnegative
end
variable B_prl(N,1) nonnegative
variable SOC_b(N,1) nonnegative
%    variable P_gen(N,1) nonnegative
variable P_gr_p(N,1) nonnegative
variable P_gr_n(N,1) nonnegative
variable P_gr_peak_A(1,1) nonnegative
variable P_gr_peak_B(1,1) nonnegative



minimize ( ...
    + sum(P_gr_p(ht_ep_array))*ts*c_ep_ht ...
    + sum(P_gr_p(nt_ep_array))*ts*c_ep_nt ...
    + sum(P_gr_p(ht_gr_array))*ts*c_gr_ht ...
    + sum(P_gr_p(nt_gr_array))*ts*c_gr_nt ...
    + P_gr_peak_A*c_peak ...
    + P_gr_peak_B*c_peak ...
    - sum(B_prl)*r_prl*ts...
    - sum(P_gr_n(ht_in_array))*ts*r_in_ht ...
    - sum(P_gr_n(nt_in_array))*ts*r_in_nt...
    -(1-debug)*C_b*(SOC_b(N)-SOC_init)*eta_b*(c_ep_ht+c_gr_ht))



subject to
% Battery constraints
%SOC_b(N)==SOC_init
if t==1
    SOC_init<=SOC_max
    SOC_init>=SOC_min
end
if no_b_losses==1
    SOC_b==[SOC_init; SOC_b(1:N-1)]+P_b/C_b*ts
else
    SOC_b==[SOC_init; SOC_b(1:N-1)]+(P_b_p*eta_b-P_b_n/eta_b)/C_b*ts
end
SOC_b+B_prl*eta_b/C_b*(PRL_time_normal+PRL_time_emerg)<=SOC_max
SOC_b-B_prl/eta_b/C_b*(PRL_time_normal+PRL_time_emerg)>=SOC_min

if no_b_losses==1
    P_b+B_prl*(1+PRL_poff)<=P_b_max
    P_b-B_prl*(1+PRL_poff)>=-P_b_max
else
    P_b_p+B_prl*(1+PRL_poff)<=P_b_max
    P_b_n+B_prl*(1+PRL_poff)<=P_b_max
end
% PRL constraints

B_prl(1)>=B_prl_next_min
B_prl(1)<=B_prl_next_max
for j=1:N-1
    if PRL_block_array(j)==PRL_block_array(j+1)
        B_prl(j)==B_prl(j+1)
    end
end
% PV constraints
%        P_gen<=P_gen_f
% Grid load constraints
P_gr_n<=max(0,P_gen_f-P_load_f) % no discharging into the grid
P_gr_peak_A>=max(P_gr_peak_prev,P_gr_p(month_A)+B_prl(month_A)*(PRL_ppeak))
P_gr_peak_B>=P_gr_p(month_B)+B_prl(month_B)*(PRL_ppeak)

%     % Trafo constraints
%        P_gr_p+B_prl <= P_tr_max
%        P_gr_n+B_prl <= P_tr_max

% Balance constraints
if no_b_losses==1
    P_gen + P_gr_p - P_gr_n == P_load_f + P_b + P_b_aux + PRL_p_loss*B_prl
else
    P_gen + P_gr_p - P_gr_n == P_load_f + P_b_p - P_b_n + P_b_aux + PRL_p_loss*B_prl
end

cvx_end

if no_b_losses==1
    P_b_p=max(P_b,0);
    P_b_n=max(-P_b,0);
end

