function [solver_out] = solve_incl_debug(no_b_losses,N,SOC_init_free,ht_ep_array,nt_ep_array,PRL_p_loss,ts,c_ep_ht,c_ep_nt,ht_gr_array,nt_gr_array,c_gr_ht,c_gr_nt,c_peak,r_prl,ht_in_array,nt_in_array,r_in_ht,r_in_nt,debug,C_b,P_b_max,eta_b,t,SOC_max,SOC_min,PRL_time_normal,PRL_time_emerg,PRL_poff,B_prl_next_min,B_prl_next_max,PRL_block_array,P_gen_f,P_load_f,P_gr_peak_prev,month_A,month_B,PRL_ppeak,P_gen,time_array,SOC_init,P_b_aux)
solve_gridconn

if strcmp(cvx_status,'Infeasible')

else
    if not(strcmp(cvx_status,'Solved'))
        debug=1;
        solve_gridconn  
        cvx_status

    end

    if  max([max(round(P_b_p).*(round(P_b_n)>round(P_b_p))) max(round(P_b_n).*(round(P_b_p)>round(P_b_n)))])>0.1    
        no_b_losses=1;
        solve_gridconn

    end
end

solver_out.P_b_p=P_b_p;
solver_out.P_b_n=P_b_n;
solver_out.B_prl=B_prl;
solver_out.SOC_b=SOC_b;
solver_out.P_gr_p=P_gr_p;
solver_out.P_gr_n=P_gr_n;
solver_out.cvx_status=cvx_status;
solver_out.cvx_optval=cvx_optval;

end