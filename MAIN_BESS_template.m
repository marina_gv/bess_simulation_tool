clear all
close all
clc

restoredefaultpath
addpath(genpath('D:\Program Files\cvx'));warning('off','MATLAB:nargchk:deprecated');

%% all input data loading
[~,~,sc]=xlsread('input data file.xlsx','scenario');
ind_scen=find(cell2mat(sc(end,2:end-2))==0);
sc(:,ind_scen+1)=[];

[~,~,ho]=xlsread('input data file.xlsx','horizon');
ind_hor=find(cell2mat(ho(end,2:end-2))==0);
ho(:,ind_hor+1)=[];

C_P_b=xlsread('input data file.xlsx','battery parameter');

ind_Bp=find(C_P_b(:,3)==0);
C_P_b(ind_Bp,:)=[];

ht_nt_hours=xlsread('input data file.xlsx','HT_hours');

T_load_profile=readtable('input data file.xlsx','Sheet','Bezug_Ruckspeisung');
T_bez=T_load_profile{:,2};
T_ruck=T_load_profile{:,3};
timestamp=T_load_profile{:,1};

[~,~,PV_profile]=xlsread('input data file.xlsx','PV profile');
T_pv=cell2mat(PV_profile(2:end,2));
P_pv_nom=cell2mat(PV_profile(2,3));
start_ht_ep=ht_nt_hours(:,2);
end_ht_ep=ht_nt_hours(:,3);
start_ht_gr=ht_nt_hours(:,4);
end_ht_gr=ht_nt_hours(:,5);
start_ht_in=ht_nt_hours(:,6);
end_ht_in=ht_nt_hours(:,7);

for isc=2:size(sc,2)-2
    disp(['scenario:' sc{1,isc}])
    scenar=cell2struct(sc(2:end,isc),sc(2:end,1),1);
    for iho=2:size(ho,2)
        disp(['horizon year:' num2str(ho{2,iho})])
        horizon_y=cell2struct(ho(3:end,iho),ho(3:end,1),1);
        
        %% intialize parameters
        % general
        ts = scenar.ts;% timestep in h
        horizon =scenar.horizon; % horizon length in h
        % start_date=('01.01.2017');
        % end_date=('24.12.2017');
        
        cvx_solver SDPT3
        
        %battery
        eta_b = scenar.eta_b; % Battery one way efficiency
        SOC_max=scenar.SOC_max;
        SOC_min=scenar.SOC_min;
        P_b_aux_norm_C_b=scenar.P_b_aux_norm_C_b;% average aux power draw normalized by battery capacity kW/kWh
        
        %PRL
        E_to_P_prl=scenar.E_to_P_prl; % Battery capacity to PRL bid power
        PRL_poff = scenar.PRL_poff; % offset power in pu (normalized by bid size)
        PRL_time_normal=scenar.PRL_time_normal; % Typically needed storage range for PRL under normal conditions, in one direction, in h
        PRL_time_emerg =scenar.PRL_time_emerg; % Reserved time for PRL, in one direction, in h
        PRL_ppeak= scenar.PRL_ppeak; % additional peak load in pu (normalized by bid size) that can be caused by PRL
        PRL_p_loss= scenar.PRL_p_loss; % average net power to compensate losses normalized by PRL bid
        PRL_bid_step=scenar.PRL_bid_step; % in kW
        
        % cost of energy product (Energieplattform)
        % no energy cost optimization desired
        c_ep_ht = scenar.c_ep_ht; % Arbeitspreis in CHF /kWh
        c_ep_nt = scenar.c_ep_nt; % Arbeitspreis in CHF /kWh
        
        % cost/remuneration infeed (Photovoltaik Steinach)
        % no energy cost optimization desired
        r_in_ht = scenar.r_in_ht; % Arbeitspreis in CHF /kWh
        r_in_nt = scenar.r_in_nt; % Arbeitspreis in CHF /kWh
        
        % cost of network product (SAK + SDL)
        c_gr_ht = scenar.c_gr_ht; % Arbeitspreis in CHF /kWh
        c_gr_nt = scenar.c_gr_nt; % Arbeitspreis in CHF /kWh
        c_peak = scenar.c_peak; % cost load peak CHF / kW / Month
        
        % SDL
        r_prl = scenar.r_prl; % remuneration PRL CHF / kW / h
        
        % additional PV installed power in kW
        add_PV=horizon_y.add_PV;
        add_load=horizon_y.add_load;
        
        %optimization
        N=horizon/ts; % number of timesteps
        
        P_pv=T_pv/P_pv_nom*add_PV; % in kW
        P_grid= T_bez/max(T_bez)*(max(T_bez)+add_load)-P_pv; % in kW % this assumes that bezug=load, which is not a fair assumption when there is considerable generation. In that case substracting pv load from bezug would be more appropriate
        
        P_load_all  = P_grid.*(P_grid>0);
        P_gen_all   =-P_grid.*(P_grid<0)-T_ruck;
       
        weekdays=weekday(timestamp)-1; % matlab sets sunday to day one, here we set monday as day one
        weekdays(weekdays==0)=7;
        hours=hour(timestamp);
        months=month(timestamp);
        years=year(timestamp);
        days=day(timestamp);
        PRL_block=floor(hours/4)+1; % 4 hour blocks PRL (1 to 6 each day)
        starting_points_MPC= [1; find(diff(PRL_block)~=0)+1];
        starting_points_MPC((starting_points_MPC+N-1)>length(timestamp))=[];
        
        T=length(P_load_all)-N+1;
        % Defintion Hoch- und Niedertarife
        ht_ep_find=false(length(hours),7);
        ht_gr_find=false(length(hours),7);
        ht_in_find=false(length(hours),7);
        for dd=1:7
            if end_ht_ep(dd)~=0
                ht_ep_find(:,dd)=(hours>=start_ht_ep(dd) & hours<end_ht_ep(dd) & weekdays==dd);
            end
            if end_ht_gr(dd)~=0
                ht_gr_find(:,dd)=(hours>=start_ht_gr(dd) & hours<end_ht_gr(dd) & weekdays==dd);
            end
            if end_ht_in(dd)~=0
                ht_in_find(:,dd)=(hours>=start_ht_in(dd) & hours<end_ht_in(dd) & weekdays==dd);
            end
        end
        ht_ep=logical(sum(ht_ep_find,2));
        nt_ep = not(ht_ep);
        
        ht_gr=logical(sum(ht_gr_find,2));
        nt_gr = not(ht_gr);
        
        ht_in=logical(sum(ht_in_find,2));
        nt_in = not(ht_in);

        %%
        P_b_var =C_P_b(:,1); % kW
        C_b_var=C_P_b(:,2); % kWh
        
        for ii =1:length(P_b_var)
            
            P_b_max = P_b_var(ii);
            C_b = C_b_var(ii);
            P_b_aux=P_b_aux_norm_C_b*C_b;
            
            nb_discrete_bid_options=min(C_b/E_to_P_prl,P_b_max/(1+PRL_poff))/PRL_bid_step;
            %% optimization grid connected mode
            P_gr_peak_prev=[];
            
            P_b_p_all=nan(N,T);
            P_b_n_all=nan(N,T);
            SOC_b_all=nan(N,T);
            P_gr_p_all=nan(N,T);
            P_gr_n_all=nan(N,T);
            P_load_f_all=nan(N,T);
            P_gen_f_all=nan(N,T);
            P_gr_peak_all=nan(1,T);
            B_prl_all=nan(N,T);
            
            unsolved=[];
            
            for j=1:length(starting_points_MPC)
                
                t=starting_points_MPC(j);
                
                %%%display(['C_BESS: ' num2str(C_b) ' P_BESS: ' num2str(P_b_max) ' progress: ' num2str(round((t/T*100))) '%'])
                status=['C_BESS: ' num2str(C_b) ' P_BESS: ' num2str(P_b_max) ' progress: ' num2str(round((t/T*100))) '%']
                
                % prepare profile
                time_array=timestamp(t:t+N-1);
                month_array=months(t:t+N-1);
                
                ht_ep_array=ht_ep(t:t+N-1);
                nt_ep_array=nt_ep(t:t+N-1);
                ht_gr_array=ht_gr(t:t+N-1);
                nt_gr_array=nt_gr(t:t+N-1);
                ht_in_array=ht_in(t:t+N-1);
                nt_in_array=nt_in(t:t+N-1);
                PRL_block_array=PRL_block(t:t+N-1);
                
                debug=0;
                no_b_losses=0;
                
                P_load_f=P_load_all(t:t+N-1); %Bezug forecast
                P_gen_f=P_gen_all(t:t+N-1); %Einspeisung forecast
                P_gen=P_gen_f;
                
                if t==1
                    SOC_init_free=1;
                    SOC_init=[];
                else
                    SOC_init_free=0;
                    SOC_init=SOC_b_all(1,t-1);
                end
                
                month_A=find(month_array==month_array(1));
                
                if length(month_A)<N
                    month_B=find(month_array==month_array(end));
                else
                    month_B=[];
                end
                
                if isempty(P_gr_peak_prev) || month_array(1)~= months(t-1)
                    P_gr_peak_prev=0;
                else
                    P_gr_peak_prev=max(P_gr_p_all(1,months(1:t-1)==month_array(1)));
                end
                
                P_gr_peak_all(t)=P_gr_peak_prev;
                
                %% solve
                
                if r_prl==0
                    B_prl_next_min=0;
                    B_prl_next_max=0;
                    [solver_out_aux] = solve_incl_debug(no_b_losses,N,SOC_init_free,ht_ep_array,nt_ep_array,PRL_p_loss,ts,c_ep_ht,c_ep_nt,ht_gr_array,nt_gr_array,c_gr_ht,c_gr_nt,c_peak,r_prl,ht_in_array,nt_in_array,r_in_ht,r_in_nt,debug,C_b,P_b_max,eta_b,t,SOC_max,SOC_min,PRL_time_normal,PRL_time_emerg,PRL_poff,B_prl_next_min,B_prl_next_max,PRL_block_array,P_gen_f,P_load_f,P_gr_peak_prev,month_A,month_B,PRL_ppeak,P_gen,time_array,SOC_init,P_b_aux);
                elseif nb_discrete_bid_options==1
                    solver_out_aux.B_prl=PRL_bid_step/2;
                elseif nb_discrete_bid_options<1
                    solver_out_aux.B_prl=0;
                else
                    B_prl_next_min=0;
                    B_prl_next_max=min(C_b/E_to_P_prl,P_b_max/(1+PRL_poff));
                    [solver_out_aux] = solve_incl_debug(no_b_losses,N,SOC_init_free,ht_ep_array,nt_ep_array,PRL_p_loss,ts,c_ep_ht,c_ep_nt,ht_gr_array,nt_gr_array,c_gr_ht,c_gr_nt,c_peak,r_prl,ht_in_array,nt_in_array,r_in_ht,r_in_nt,debug,C_b,P_b_max,eta_b,t,SOC_max,SOC_min,PRL_time_normal,PRL_time_emerg,PRL_poff,B_prl_next_min,B_prl_next_max,PRL_block_array,P_gen_f,P_load_f,P_gr_peak_prev,month_A,month_B,PRL_ppeak,P_gen,time_array,SOC_init,P_b_aux);
                end
                % try lower discrete value for PRL bid
                B_prl_next_min=floor(solver_out_aux.B_prl(1)/PRL_bid_step)*PRL_bid_step;
                B_prl_next_max=B_prl_next_min;
                [solver_out_low] = solve_incl_debug(no_b_losses,N,SOC_init_free,ht_ep_array,nt_ep_array,PRL_p_loss,ts,c_ep_ht,c_ep_nt,ht_gr_array,nt_gr_array,c_gr_ht,c_gr_nt,c_peak,r_prl,ht_in_array,nt_in_array,r_in_ht,r_in_nt,debug,C_b,P_b_max,eta_b,t,SOC_max,SOC_min,PRL_time_normal,PRL_time_emerg,PRL_poff,B_prl_next_min,B_prl_next_max,PRL_block_array,P_gen_f,P_load_f,P_gr_peak_prev,month_A,month_B,PRL_ppeak,P_gen,time_array,SOC_init,P_b_aux);
                
                % try higher discrete value for PRL bid
                B_prl_next_min=ceil(solver_out_aux.B_prl(1)/PRL_bid_step)*PRL_bid_step;
                B_prl_next_max=B_prl_next_min;
                [solver_out_high] = solve_incl_debug(no_b_losses,N,SOC_init_free,ht_ep_array,nt_ep_array,PRL_p_loss,ts,c_ep_ht,c_ep_nt,ht_gr_array,nt_gr_array,c_gr_ht,c_gr_nt,c_peak,r_prl,ht_in_array,nt_in_array,r_in_ht,r_in_nt,debug,C_b,P_b_max,eta_b,t,SOC_max,SOC_min,PRL_time_normal,PRL_time_emerg,PRL_poff,B_prl_next_min,B_prl_next_max,PRL_block_array,P_gen_f,P_load_f,P_gr_peak_prev,month_A,month_B,PRL_ppeak,P_gen,time_array,SOC_init,P_b_aux);
                
                if solver_out_high.cvx_optval<solver_out_low.cvx_optval
                    solver_out=solver_out_high;
                else
                    solver_out=solver_out_low;
                end
                
                P_b_p=solver_out.P_b_p;
                P_b_n=solver_out.P_b_n;
                B_prl=solver_out.B_prl;
                SOC_b=solver_out.SOC_b;
                P_gr_p=solver_out.P_gr_p;
                P_gr_n=solver_out.P_gr_n;
                cvx_status=solver_out.cvx_status;
                cvx_optval=solver_out.cvx_optval;
                
                
                if (strcmp(cvx_status,'Solved') || strcmp(cvx_status,'Inaccurate/Solved'))  &&  max([max(round(P_b_p).*(round(P_b_n)>round(P_b_p))) max(round(P_b_n).*(round(P_b_p)>round(P_b_n)))])<0.1
                    
                    P_b_p_all(:,t)=P_b_p;
                    P_b_p_all(1,t:t+N-1)=P_b_p(1:N);
                    
                    P_b_n_all(:,t)=P_b_n;
                    P_b_n_all(1,t:t+N-1)=P_b_n(1:N);
                    
                    SOC_b_all(:,t)=SOC_b;
                    SOC_b_all(1,t:t+N-1)=SOC_b(1:N);
                    
                    B_prl_all(:,t)=B_prl;
                    B_prl_all(1,t:t+N-1)=B_prl(1:N);
                    
                    P_gr_p_all(:,t)=P_gr_p;
                    P_gr_p_all(1,t:t+N-1)=P_gr_p(1:N);
                    
                    P_gr_n_all(:,t)=P_gr_n;
                    P_gr_n_all(1,t:t+N-1)=P_gr_n(1:N);
                    
                    P_load_f_all(:,t)=P_load_f;
                    P_load_f_all(1,t:t+N-1)=P_load_f(1:N);
                    
                    P_gen_f_all(:,t)=P_gen_f;
                    P_gen_f_all(1,t:t+N-1)=P_gen_f(1:N);
                    
                else
                    unsolved=[unsolved t];
                end
          
            end
            
            save([sc{1,isc} '\' 'C_BESS_' num2str(C_b) '_P_BESS_' num2str(P_b_max) '_year_' num2str(ho{2,iho})])
            
            figure
            hold on
            plot(timestamp(1:T),SOC_b_all(1,1:T)+B_prl_all(1,1:T)/C_b*eta_b*(PRL_time_normal+PRL_time_emerg))
            plot(timestamp(1:T),SOC_b_all(1,1:T)-B_prl_all(1,1:T)/C_b/eta_b*(PRL_time_normal+PRL_time_emerg))
            plot(timestamp(1:T),SOC_b_all(1,1:T))
            legend('SOC + prl margin(normal+emerg)', 'SOC - prl margin(normal+emerg)','SOC')
            
            
            figure
            hold on
            plot(timestamp(1:T),(P_b_p_all(1,1:T)+B_prl_all(1,1:T)*(1+PRL_poff))/P_b_max)
            plot(timestamp(1:T),(-P_b_p_all(1,1:T)-B_prl_all(1,1:T)*(1+PRL_poff))/P_b_max)
            plot(timestamp(1:T),P_b_p_all(1,1:T)/P_b_max)
            plot(timestamp(1:T),-P_b_n_all(1,1:T)/P_b_max)
            legend( 'P batt char + prl margin', 'P batt dischar - prl margin','P batt char p.u.', 'P batt disch')
            
            figure
            subplot(2,1,1)
            plot(timestamp(1:T),P_load_f_all(1,1:T))
            hold on
            plot(timestamp(1:T),P_gr_p_all(1,1:T))
            legend('load','grid')
            subplot(2,1,2)
            plot(timestamp(1:T),P_b_p_all(1,1:T)-P_b_n_all(1,1:T))
            hold on
            plot(timestamp(1:T),P_gr_p_all(1,1:T)-P_b_p_all(1,1:T)+P_b_n_all(1,1:T)-P_load_f_all(1,1:T)+P_gen_f_all(1,1:T))
            legend('battery','balance')
            
            figure
            yyaxis left
            plot(timestamp(1:T),B_prl_all(1,1:T))
            yyaxis right
            
            legend('PRL bid')
            
            figure
            hold on
            plot(timestamp(1:T),P_gr_p_all(1,1:T))
            plot(timestamp(not(isnan(P_gr_peak_all))),P_gr_peak_all(not(isnan(P_gr_peak_all))))
            stairs(timestamp(1:T),P_gr_p_all(1,1:T)+B_prl_all(1,1:T)*PRL_ppeak)
            legend('grid import','peak','grid import + PRL bid')
            
        end
    end
end
